$("text-area").on('keyup', function (e) {
    if (e.keyCode === 13) {
      var chatEntry = document.getElementByTagName('text-area');
      var chatBody = document.getElementByClassName('msg-insert');

      chatBody.innerHTML += chatEntry.innerHTML;
      chatEntry.value = "";
    }
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
      print.value = "";
      erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin') {
      print.value = Math.round(Math.sin(print.value * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.round(Math.log10(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'tan') {
      print.value = Math.round(Math.tan(print.value * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else {
      if (erase) {
        print.value = "";
        erase = false;
      }
      print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//SELECT
var theme = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},{"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},{"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},{"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},{"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},{"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},{"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},{"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},{"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},{"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},{"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];
var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
localStorage.setItem('theme', JSON.stringify(theme));
localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));

$(document).ready(function() {
  $('.my-select').select2({
    data: JSON.parse(localStorage.getItem('theme'));
  });

  $('.apply-button').on('click', function () {
      theme = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];
      $('body').css({"backgroundColor": theme['bcgColor']});
      $('.text-center').css({"color": theme['fontColor']});
      localStorage.setItem('selectedTheme', JSON.stringify(theme));
  });

});
