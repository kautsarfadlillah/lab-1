$("textarea").keypress(function(e){
    if (e.keyCode == 13 && !e.shiftKey)
    {        
        e.preventDefault();
        var txt = $(this).val();
        $(".msg-insert").append("<p class='msg-send'>"+txt+"</p>");
        $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast'); //auto scroll to the latest chat
        $(this).val("");
    }
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
      print.value = "";
      erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin') {
      print.value = Math.round(Math.sin(evil(print.value) * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.round(Math.log10(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'tan') {
      print.value = Math.round(Math.tan(print.value * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else {
      if (erase) {
        print.value = "";
        erase = false;
      }
      print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//SELECT
var themes = [{"id":1,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},{"id":2,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},{"id":3,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},{"id":4,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},{"id":5,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},{"id":6,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},{"id":7,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},{"id":8,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},{"id":9,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},{"id":10,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},{"id":11,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];
var selectedTheme = {"id":4,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"};
localStorage.setItem('themes', JSON.stringify(themes));
localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));

$(document).ready(function() {
  $('.my-select').select2({
    'data': JSON.parse(localStorage.themes)
  });

  var selectedTheme = JSON.parse(localStorage.selectedTheme);
  $('body').css({"backgroundColor": selectedTheme['bcgColor']});
  $('.text-center').css({"color": selectedTheme['fontColor']});

  $('.apply-button').on('click', function () {
      var themeVal = $('.my-select').val();
      var themes = JSON.parse(localStorage.themes);
      var selectedTheme;

      for (i = 0; i < themes.length; i++) {
        if (themeVal == themes[i].id) {
          selectedTheme = themes[i];
          break;
        }
      }
      
      $('body').css({"backgroundColor": selectedTheme['bcgColor']});
      $('.text-center').css({"color": selectedTheme['fontColor']});

      localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  });

});