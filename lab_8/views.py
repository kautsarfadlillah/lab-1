from django.shortcuts import render

# Create your views here.
def index(request):
    response = {'author':'Kautsar Fadlillah'}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
